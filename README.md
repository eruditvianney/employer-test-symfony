# employer-test-symfony
# CRUD EMPLOYE
# Instruction Installation

# 1 Installation composer
composer insall

# 2 Modifier la configuration de la base de donnees dans
# le fichier .env et apres lancer la commande suivantes
php bin/console doctrine:database:create

# 3 Creation et migration des tables
php bin/console d:s:u --force

# 4 creation d'utilisateur par defaut 
php bin/console app:create-user

# 5 Demarrer le server
php -S 127.0.0.1:3000 -t public

# 7 Ouvrir votre navigateur et tapper cet url
http://localhost:3000

# 8 compte pardefaut d'utilisateur

email ==> mecene@test.fr
password ==> user123

# ##pour l'API platform  voici l'url pour voir tous les endpoints ##

http://localhost:3000/api

