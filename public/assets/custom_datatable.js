$(function (e) {
    $('#experienceTable').DataTable({
        //Internationalisation de dataTable
        "language": {
            "sProcessing": "Traitement en cours...",
            "sSearch": "Recherche : ",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sInfo": "Affichage de l'élément 1 à 10 sur TOTAL éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré de MAX éléments au total)",
            "sInfoPostFix": "",
            "sLoadingRecords": "Chargement en cours...",
            "sZeroRecords": "Aucun élément à afficher",
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "oPaginate": {
                "sFirst": "Premier",
                "sPrevious": "Précédent",
                "sNext": "Suivant",
                "sLast": "Dernier"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            }
        },
    });
});