<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{

    private $manager;
    private $encoders;
    public function __construct(EntityManagerInterface $manager, UserPasswordEncoderInterface $encoders)
    {
        $this->manager = $manager;
        $this->encoders = $encoders;
        parent::__construct();
    }
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-user';

    protected function configure()
    {
        $this->setDescription('Creation utilisateur.')->setHelp('Commade pour la creation utilisateur');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = new User();
        $user->setUsername('Mecene')
        ->setEmail('mecene@test.fr')
        ->setPassword($this->encoders->encodePassword($user, 'user123'))
        ->setRoles(['ROLE_ADMIN']);
        $this->manager->persist($user); 
        $this->manager->flush();

        $output->writeln([
            '============',
            '===> Creation utilisateur ok',
            '============'
        ]);
        return Command::SUCCESS;
    }
}