<?php

namespace App\Controller;

use App\Entity\Employer;
use App\Form\EmployerType;
use App\Repository\EmployerRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\FormHandler\EmployerFormHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/employer", name="employer_")
 */
class EmployerController extends AbstractController
{

    protected $em;
    protected $repository;
    public function __construct(EntityManagerInterface $em, EmployerRepository $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $employers = $this->repository->findAll();
        return $this->render('employer/index.html.twig', [
            'employers' => $employers,
            'current_menu' => 'employer',
        ]);
    }


    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request)
    {
        $employer = new Employer();
        $form = $this->createForm(EmployerType::class, $employer);
        $formHandler = new EmployerFormHandler($form, $request, $this->em);

        if (!is_null($formHandler->process())) {
            $this->addFlash('success','Enregistrement ok');
            return $this->redirectToRoute('employer_index');
        }
        return $this->render('/employer/new.html.twig', [
            'current_menu' => 'employer',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function update(Request $request, Employer $employer)
    {
        $form = $this->createForm(EmployerType::class, $employer);
        $formHandler = new EmployerFormHandler($form, $request, $this->em);

        if (!is_null($formHandler->process())) {
            $this->addFlash('success','Modifiction ok');
            return $this->redirectToRoute('employer_index');
        }

        return $this->render('/employer/edit.html.twig', [
            'current_menu' => 'employer',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods="DELETE")
     */
    public function delete(Request $request, Employer $employer)
    {
        if ($this->isCsrfTokenValid('delete' .$employer->getId(), $request->get('_token'))) {
            $this->em->remove($employer);
            $this->em->flush();
            $this->addFlash('success','Suppression ok');
            return $this->redirectToRoute('employer_index');
        }

        return $this->redirectToRoute('employer_index');
    }
}
