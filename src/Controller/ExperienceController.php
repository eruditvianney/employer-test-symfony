<?php

namespace App\Controller;

use App\Entity\Experience;
use App\Form\ExperienceType;
use App\Repository\ExperienceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\FormHandler\ExperienceFormHandler;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/experience", name="experience_")
 */
class ExperienceController extends AbstractController
{
    protected $em;
    protected $repository;
    public function __construct(EntityManagerInterface $em, ExperienceRepository $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $experiences = $this->repository->findAll();
        return $this->render('experience/index.html.twig', [
            'experiences' => $experiences,
            'current_menu' => 'experience',
        ]);
    }


    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request)
    {
        $experience = new Experience();
        $form = $this->createForm(ExperienceType::class, $experience);
        $formHandler = new ExperienceFormHandler($form, $request, $this->em);

        if (!is_null($formHandler->process())) {
            $this->addFlash('success','Enregistrement ok');
            return $this->redirectToRoute('experience_index');
        }
        return $this->render('/experience/new.html.twig', [
            'current_menu' => 'experience',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function update(Request $request, Experience $experience)
    {
        $form = $this->createForm(ExperienceType::class, $experience);
        $formHandler = new ExperienceFormHandler($form, $request, $this->em);

        if (!is_null($formHandler->process())) {
            $this->addFlash('success','Modifiction ok');
            return $this->redirectToRoute('experience_index');
        }

        return $this->render('/experience/edit.html.twig', [
            'current_menu' => 'experience',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods="DELETE")
     */
    public function delete(Request $request, Experience $experience)
    {
        if ($this->isCsrfTokenValid('delete' .$experience->getId(), $request->get('_token'))) {
            $this->em->remove($experience);
            $this->em->flush();
            $this->addFlash('success','Suppression ok');
            return $this->redirectToRoute('experience_index');
        }

        return $this->redirectToRoute('experience_index');
    }
}
