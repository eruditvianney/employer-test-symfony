<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login()
    {
        return $this->render('security/login.html.twig');
    }

      /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        
    }

    /**
     * Redirect users after login based on the granted ROLE
     * @Route("/login/redirect", name="login_redirect")
     */
    public function loginRedirect()
    {
        if($this->isGranted('ROLE_ADMIN'))
        {
            return $this->redirectToRoute('employer_index');
        }
            
        return $this->redirectToRoute('home');
    }
}
