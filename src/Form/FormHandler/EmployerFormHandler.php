<?php


namespace App\Form\FormHandler;

use App\Entity\Employer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class EmployerFormHandler
{
    private $form;
    private $request;
    private $em;

    public function __construct(FormInterface $form, Request $request, EntityManagerInterface $em)
    {
        $this->form = $form;
        $this->request = $request;
        $this->em = $em;
    }

    public function process()
    {
        $this->form->handleRequest($this->request);
        if ($this->form->isSubmitted() && $this->form->isValid()){
            return $this->onSuccess();
        }
        return null;
    }

    private function onSuccess()
    {
        $employer = $this->form->getData();
        $this->em->persist($employer);
        $this->em->flush();
        return $employer;
    }
}
