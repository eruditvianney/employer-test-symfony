<?php


namespace App\Form\FormHandler;

use App\Entity\Experience;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class ExperienceFormHandler
{
    private $form;
    private $request;
    private $em;

    public function __construct(FormInterface $form, Request $request, EntityManagerInterface $em)
    {
        $this->form = $form;
        $this->request = $request;
        $this->em = $em;
    }

    public function process()
    {
        $this->form->handleRequest($this->request);
        if ($this->form->isSubmitted() && $this->form->isValid()){
            return $this->onSuccess();
        }
        return null;
    }

    private function onSuccess()
    {
        $experience = $this->form->getData();
        $this->em->persist($experience);
        $this->em->flush();
        return $experience;
    }
}
